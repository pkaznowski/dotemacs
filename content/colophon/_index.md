+++
title = "Colophon"
author = ["Piotr Kaznowski"]
tags = ["colophon"]
draft = false
weight = 998
anchor = "colophon"
+++

This site is made with [Hugo static site generator](https://gohugo.io/) and deployed on [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/). All posts are written in Emacs' [Org mode](https://orgmode.org) and are exported with [Ox-hugo](https://ox-hugo.scripter.co/). The theme is slightly modified [Kraiklyn](https://themes.gohugo.io/kraiklyn/).

Let's automate insertion of export settings. The most tricky part is to calculate current entry weight. I'll use [org-element API](https://orgmode.org/worg/dev/org-element-api.html):

<a id="code-snippet--start1"></a>
```emacs-lisp
(defun pk--calculate-hugo-weight (here)
  "Parse buffer looking for EXPORT_HUGO_WEIGHT properties before the heading
  at point. Return the last property increased to the next decimal value"
  (replace-regexp-in-string
   "[0-9]$" "0"
   (number-to-string
    (+
     10
     (string-to-number
      (or
       (first
        (reverse
         (org-element-map (org-element-parse-buffer 'headline) 'headline
           (lambda (hl) (when (<= (org-element-property :end hl) here)
                          (org-element-property :EXPORT_HUGO_WEIGHT hl))))))
       "0"))))))
```

I will use headline's first tag to indicate exporting section. Since `Kraiklyn` theme imposes usage of `_index.md` files for cross-references I will export every subtree to that file placed in the proper section. The last thing is `anchor` property which is used for navigation (it will have the same value as section by default). Let's put all elements together:

<a id="code-snippet--start2"></a>
```emacs-lisp
(defun pk/insert-hugo-properties ()
  (interactive)
  (let* ((hdlc (org-heading-components))
         (headline (nth 4 hdlc))
         (tags (nth 5 hdlc))
         (section (if tags
                      (first (split-string tags ":" t))
                    (downcase headline)))
         (here (point))
         (weight (pk--calculate-hugo-weight (org-entry-beginning-position))))
    (org-entry-put here "EXPORT_HUGO_SECTION" section)
    (org-entry-put here "EXPORT_FILE_NAME" "_index.md")
    (org-entry-put here "EXPORT_HUGO_WEIGHT" weight)
    (org-entry-put here "EXPORT_HUGO_CUSTOM_FRONT_MATTER" (format ":anchor %s" section))))
```
