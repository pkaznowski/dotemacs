+++
title = "Basic keybindings and maps"
author = ["Piotr Kaznowski"]
tags = ["kbd"]
draft = false
weight = 120
anchor = "kbd"
+++

## Define-keys-in {#define-keys-in}

```emacs-lisp
(defun define-keys-in (key-map kbd-cmd-alist)
  "Define keys for KEY-MAP declared in KBD-CMD-ALIST
where keys are cars and commands are cadrs of cons cells.

Example:
(define-keys-in lisp-interaction-mode-map
                '((\"C-c p\" previous-line)
                  (\"C-c n\" next-line))"
  ;; (dolist (cell (eval kbd-cmd-alist))
  ;;   (define-key (eval key-map)
  ;;     (kbd (car cell)) (cadr cell)))
  (dolist (cell kbd-cmd-alist)
      (define-key key-map
        (kbd (car cell)) (cadr cell))))
```


## Define custom maps {#define-custom-maps}

```emacs-lisp
(global-unset-key (kbd "<menu>"))

(eval-and-compile
  (mapc #'(lambda (entry)
            (define-prefix-command (cdr entry))
            (bind-key (car entry) (cdr entry)))
        '(("M-SPC"      . pk/m-spc-map)
          ("<menu> SPC" . pk/menu-spc-map)
          ("C-x p"      . pk/ctrl-x-p-map)
          ("C-x x"      . pk/ctrl-x-x-map))))
```


## Meta map {#meta-map}

<a id="code-snippet--meta map"></a>
```emacs-lisp
(eval-and-compile
  (global-unset-key (kbd "M-a"))
  (global-unset-key (kbd "M-e"))
  (global-unset-key (kbd "M-e"))
  (global-unset-key (kbd "M-h"))

  (define-keys-in global-map
    '(("M-<f5>" (lambda ()
                  (interactive)
                  (revert-buffer nil t)))
      ("M-'" set-mark-command)
      ("M-`" dabbrev-expand)
      ("M-," forward-paragraph)
      ("M-." forward-sentence)
      ("M-8" backward-paragraph)
      ("M-;" end-of-line)
      ("M-D" kill-sentence)
      ("M-\\" fill-paragraph)
      ("M-\"" exchange-point-and-mark)
      ("M-]" next-buffer)
      ("M-[" previous-buffer)
      ("M-a" mark-whole-buffer)
      ("M-b" ibuffer)
      ("M-c" pk/copy-line-or-region)
      ("M-e" delete-char)
      ("M-ę" eval-buffer)
      ("M-f" backward-kill-word)
      ("M-g" kill-region)
      ("M-G" pk/delete-region-dwim)
      ("M-h" beginning-of-line)
      ("M-i" previous-line)
      ("M-j" backward-char)
      ("M-k" next-line)
      ("M-l" forward-char)
      ("M-m" backward-sentence)
      ("M-n" switch-to-buffer)
      ("M-N" counsel-find-file)
      ("M-o" forward-word)
      ("M-ó" occur)
      ("M-p" comment-dwim)
      ("M-P" comment-line)
      ("M-q" kill-this-buffer)
      ("M-r" delete-backward-char)
      ("M-s" save-buffer)
      ("M-u" backward-word)
      ("M-v" yank)
      ("M-W" make-frame)
      ("M-Z" pk/projectile-switch-project->treemacs)
      ("M-¬" switch-window-then-swap-buffer))))

(when (string= system-type "gnu/linux")
  (global-set-key (kbd "M-S-<f4>") 'save-buffers-kill-emacs))
```


## Meta SPC map {#meta-spc-map}

<a id="code-snippet--meta space map"></a>
```emacs-lisp
(define-keys-in pk/m-spc-map
  '(("w" widen)
    ("i" ispell)
    ("b" bookmark-jump)
    ("”" bookmark-set)
    ("B" bookmark-bmenu-list)
    ("n r" narrow-to-region)
    ("n d" narrow-to-defun)
    ("j" set-justification-full)
    ("g" pk/org-goto-dwim)))
```


## Ctrl map {#ctrl-map}

<a id="code-snippet--ctrl map"></a>
```emacs-lisp
(define-keys-in global-map
              '(("C--" text-scale-decrease)
                ("C-=" text-scale-increase)
                ("C-0" text-scale-adjust)
                ("C-h c" describe-face)
                ("C-r" transpose-chars)
                ("C-SPC" scroll-up)
                ("C-S-SPC" scroll-down)))
```


## Menu map {#menu-map}

<a id="code-snippet--menu map"></a>
```emacs-lisp
(when (string= system-type "gnu/linux")
  (define-keys-in global-map
    '(("<menu> 1" delete-other-windows)
      ("<menu> 4" clone-indirect-buffer-other-window)
      ("<menu> 0" delete-window)
      ("<menu> w" outline-next-visible-heading)
      ("<menu> e" outline-previous-visible-heading)
      ("<menu> f" org-forward-heading-same-level)
      ("<menu> r" org-backward-heading-same-level)
      ("<menu> d" (lambda () (interactive) (dired default-directory)))
      ("<menu> c u" upcase-word)
      ("<menu> c d" downcase-word)
      ("<menu> c D" downcase-region)
      ("<menu> c U" upcase-region)
      ("<menu> c c" capitalize-word)
      )))
```


## Other builtin maps {#other-builtin-maps}


### diff mode map {#diff-mode-map}

```emacs-lisp
(use-package diff-mode
  :ensure nil
  :defer t
  :config
  (define-key diff-mode-map (kbd "M-k") 'next-line)
  (define-key diff-mode-map (kbd "M-g") 'diff-hunk-kill))
```


### Info mode map {#info-mode-map}

```emacs-lisp
(use-package Info-mode
  :ensure nil
  :defer t
  :config
  (define-keys-in Info-mode-map
    '(("a" beginning-of-line)
      ("c" clone-buffer)
      (";" end-of-line)
      ("k" previous-line)
      ("j" next-line))))
```


### Ielm -> Comint mode map {#ielm-comint-mode-map}

```emacs-lisp
(use-package comint
  :ensure nil
  :defer t
  :config
  (define-keys-in comint-mode-map
    '(("C-p" comint-previous-input)
      ("C-n" comint-next-input)
      ("C-c p" comint-previous-prompt)
      ("C-c n" comint-next-prompt)
      ("C-c C-k" comint-clear-buffer)
      ("M-n" ivy-switch-buffer))))
```
