+++
title = "Intro"
author = ["Piotr Kaznowski"]
tags = ["intro"]
draft = false
weight = 100
anchor = "intro"
+++

## Why Emacs config in an .org file? {#why-emacs-config-in-an-dot-org-file}

While "literate programming" approach is popular among Emacs/Org-mode users I chose Org-mode mainly because of it powerful outline system which enables me to easily navigate, search & find, comment, wiki, etc. every hunk of code in my Emacs config.


## How it works? {#how-it-works}


### Tangling files {#tangling-files}

The whole config code is split into smaller files which may be easily turn off for debugging purposes and, hopefully, will help me to avoid (in)famous dot emacs bankruptcy.

Each section (under org level 1 header) is being tangled to a different file. Currently the structure looks like this:

```text
~/.emacs.d:
...
├── init.el
├── config
│   ├── dot-appearance.el
│   ├── dot-basic-pkgs.el
│   ├── dot-custom-modes.el
│   ├── dot-env.el
│   ├── dot-kbd.el
│   ├── dot-my-functions.el
│   ├── dot-org.el
│   └── dot-packages.el
...
```

In each section I set tangle file name in root header properties, e.g.:

```text
:PROPERTIES:
:header-args: :tangle (expand-file-name "init.el" user-emacs-directory) :comments yes :results silent
:END:
```

Sometimes I don't want to put everything into one source block, then I take advantage of `noweb` functionality, using `:noweb` and `:noweb-ref` keys in the block's header args.

I added a custom function to `after-save-hook` in this file which automatically tangles _only_ the blocks for the section at point. It is very useful since tangling big amount of blocks may take too long to keep it automaticized after each saving the file. You will find the function under [org custom functions](./.org) section.


### Script to update local files {#script-to-update-local-files}

I work on several machines thus I need keeping my local `~/.emacs.d` folders synced. For that purpose I launch `tangle-config-org.sh` script after local `dotemacs` repo is updated. The script is simple and uses emacs `--batch` option:

```text
#!/bin/bash
emacs -Q --batch --eval "(let
      ((my-config-dir \"~/.emacs.d/config\"))
      (require 'ob-tangle)
      (with-current-buffer (find-file-noselect \"config.org\")
      (org-babel-tangle)))"
```


## Disclaimer {#disclaimer}

This config file was growing for some time. There are some "ancient" bits which were gathered from or inspired by multiple sources. When I was inserting them into my config I did't always think about putting credentials. However I try to be consistent with custom functions naming -- all prefixed with `pk` are written by me. Others might have been copied from somewhere else.
