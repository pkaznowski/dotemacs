+++
title = "Appearance"
author = ["Piotr Kaznowski"]
draft = false
weight = 130
anchor = "appearance"
+++

## Font {#font}

```emacs-lisp
(cond
 ((member "JetBrains Mono" (font-family-list))
  (set-face-attribute 'default nil
                      :foundry "JB"
                      :family "JetBrains Mono"
                      :height 97))
 ((member "Monoid" (font-family-list))
  (set-face-attribute 'default nil
                      :foundry "PfEd"
                      :family "Monoid"
                      :height 90))
 ;; ((member "Lekton" (font-family-list))
 ;;  (set-face-attribute 'default nil
 ;;                      :foundry "PfEd"
 ;;                      :family "Lekton"
 ;;                      :height 130))
 ;; ((member "Overpass Mono" (font-family-list))
 ;;  (set-face-attribute 'default nil
 ;;                      :family "Overpass Mono"
 ;;                      :height 105))
 ;; ((member "Fira Mono" (font-family-list))
 ;;  (set-face-attribute 'default nil
 ;;                      :family "Fira Mono"
 ;;                      :height 96))
 ((member "Iosevka" (font-family-list))
  (set-face-attribute 'default nil
                      :family "Iosevka"
                      :height 120))
 ((member "Inconsolata" (font-family-list))
  (set-face-attribute 'default nil
                      :family "Inconsolata"
                      :height 125))
 ((member "Meslo LG M" (font-family-list))
  (set-face-attribute 'default nil
                      :family "Meslo LG M"
                      :height 105)))
```


## Theme {#theme}

```emacs-lisp
(setq custom-safe-themes t)
(let ((gui (display-graphic-p)))
  (cond ((and gui (file-exists-p "~/.emacs.d/newgruv-theme.el")) (load-theme 'newgruv))
        ((and gui (file-exists-p "~/.emacs.d/jazz-theme.el")) (load-theme 'jazz))
        (gui (load-theme 'pastel))
        ((file-exists-p "~/.emacs.d/blackwhite-theme.el") (load-theme 'blackwhite))
        (t (load-theme 'pastel-client))))
```

```emacs-lisp
(defun pk/theme-toggle ()
  (interactive)
  (let* ((theme-pairs '((pastel . pastel-light)
                        (pastel-light . pastel)))
         (current-theme (car custom-enabled-themes))
         (new-theme
          (assoc-default current-theme theme-pairs))
         (agenda (get-buffer "*Org Agenda*")))
    (disable-theme current-theme)
    (load-theme new-theme)
    (if (eq new-theme 'pastel-light)
        (setq-default cursor-type 'box)
      (setq-default cursor-type 'bar))
    (pk/set-org-todo-faces)
    (when (boundp org-bullets-mode)
      (progn
        (message "ORG BULLETS fontifying buffers...")
        (dolist (ob (org-buffer-list))
          (with-current-buffer ob
            (org-bullets--fontify-buffer)))
        (message "ORG BULLETS fontifying buffers...done")))
    (when (member agenda (buffer-list))
      (with-current-buffer agenda
        (org-agenda-redo-all)))
    (if (eq current-theme 'pastel)
        (global-hl-line-mode 1)
      ;; FIX! it's a temporary solution (yamol--mode-line... should work for all themes
      (when (featurep 'yamol)
        (yamol--mode-line-update-faces))
      (global-hl-line-mode -1))
    (message "%s theme loaded" new-theme)))

(global-set-key (kbd "C-<f6>") #'pk/theme-toggle)
```


## Minibuffer setup {#minibuffer-setup}

```emacs-lisp
(defun pk/minibuff-custom-settings ()
  (let (bg-color)
    (make-local-variable 'face-remapping-alist)
    (cond ((eq (car custom-enabled-themes) 'pastel-light)
           (setq bg-color ptl_dark1))
          ((eq (car custom-enabled-themes) 'pastel)
           (setq bg-color pt_dark1))
          (t (setq bg-color (face-attribute 'default :background))))
    (add-to-list 'face-remapping-alist
                 `(default (:height 0.9
                                    :background ,bg-color)))))

(when (display-graphic-p)
  (add-hook 'minibuffer-setup-hook #'pk/minibuff-custom-settings))
```


## Highlight elisp code {#highlight-elisp-code}

```emacs-lisp
(use-package highlight-defined
  :config
  (add-hook 'emacs-lisp-mode-hook 'highlight-defined-mode))
```


## Transparency {#transparency}

```emacs-lisp
(set-frame-parameter (selected-frame) 'alpha '(90 . 90))
(add-to-list 'default-frame-alist '(alpha . (90 . 90)))

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(90 . 90) '(100 . 100)))))

(global-set-key (kbd "<menu> t") #'toggle-transparency)
```


## Custom font lock keywords {#custom-font-lock-keywords}

```emacs-lisp
(defun pk/font-lock-keywords ()
  "Add custom keywords and their faces to font-lock-keywords list."
  (font-lock-add-keywords
   nil
   '(("\\<\\(fixme\\|bug\\|BUG\\|FIXME\\|fix\\|FIX\\):" 1
      `(:foreground "firebrick1"
                    :weight bold
                    :box (:line-width 1 :style released-button)
                    :height ,(face-attribute 'default :height))
      ;; `(:inherit org-todo
      ;;            :height ,(face-attribute 'default :height))
      t)
     ("\\<\\(todo\\|TODO\\):" 1
      `(:inherit warning
                 :weight bold
                 :box (:line-width 1 :style released-button)
                 :height ,(face-attribute 'default :height))
      t)
     ("\\<\\(done\\|DONE\\):" 1
      `(:inherit org-done
                 :height ,(face-attribute 'default :height))
      t)
     ("\\<\\(note\\|NOTE\\):" 1
      `(:foreground "steel blue"
                    :weight bold
                    :box (:line-width 1 :style released-button)
                    :height ,(face-attribute 'default :height))
      t)
     ("\\<\\(check\\|CHECK\\):" 1
      `(:foreground "olive drab"
                   :weight bold
                   :box (:line-width 1 :style released-button)
                   :height ,(face-attribute 'default :height))
      t))))

(add-hook 'prog-mode-hook #'pk/font-lock-keywords)
```
