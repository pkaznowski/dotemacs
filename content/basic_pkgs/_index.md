+++
title = "Basic packages and functions"
author = ["Piotr Kaznowski"]
tags = ["basic-pkgs"]
draft = false
weight = 150
anchor = "basic_pkgs"
+++

## Advices, tunings etc. {#advices-tunings-etc-dot}


### Kill region advice {#kill-region-advice}

```emacs-lisp
(defadvice kill-region (before slick-cut activate compile)
    "when called interactively with no active region, kill a single line instead."
    (interactive
     (if mark-active
         (list (region-beginning) (region-end))
       (list (line-beginning-position)
             (line-beginning-position 2)))))
```


### Kill buffer and window {#kill-buffer-and-window}

```emacs-lisp
(defun pk/kill-buffer-and-window ()
  (interactive)
  (progn
    (kill-this-buffer)
    (when (> (length (window-list)) 1)
      (delete-window))))

(global-set-key (kbd "M-Q") #'pk/kill-buffer-and-window)
```


### Copy line or region {#copy-line-or-region}

```emacs-lisp
(defun pk/copy-line-or-region ()
  "If region is not active, copy whole line; otherwise copy the region."
  (interactive)
  (if (use-region-p)
      (let ((beg (region-beginning))
            (end (region-end)))
        (kill-ring-save beg end)
        (message "Region %d..%d copied to kill ring" beg end))
    (kill-ring-save (line-beginning-position) (line-end-position))
    (message "Line %d copied to kill-ring" (line-number-at-pos))))
```


## Auto complete {#auto-complete}

```emacs-lisp
(use-package auto-complete
  :demand ;; check: ?
  :config
  (ac-config-default)
  (ac-flyspell-workaround) ;; makes ac faster?
  ;; (add-hook 'conf-space-mode-hook 'auto-complete-mode)
  ;; (add-hook 'LaTeX-mode-hook 'auto-complete-mode)
  )
```


## Avy {#avy}

```emacs-lisp
(use-package avy
  :bind ("C-;" . avy-goto-char-timer))
```


## Counsel, Swiper, Ivy {#counsel-swiper-ivy}

```emacs-lisp
(use-package counsel
  :bind
  ("M-x" . counsel-M-x)
  ("C-h f" . counsel-describe-function)
  ("<f1> f" . counsel-describe-function)
  ("C-h v" . counsel-describe-variable)
  ("<f1> v" . counsel-describe-variable)
  ("<f1> c" . counsel-describe-face)
  ("<f1> m" . counsel-describe-mode)
  ("C-x c-f" . counsel-find-file)
  ("M-N" . counsel-find-file)
  )

(use-package swiper
  :bind
  ("C-s" . swiper-isearch)
  ("C-S-s" . swiper))

(use-package ivy
  :diminish ivy-mode
  :init (ivy-mode 1)
  :config
  (defun pk/ivy-next-dwim ()
    (interactive)
    (if (eq this-command last-command)
        (ivy-next-line)
      (ivy-partial)))

  (setq ivy-magic-slash-non-match-action 'nil)

  (dolist (cell '(("<tab>" pk/ivy-next-dwim)
                  ("C-n" ivy-next-history-element)
                  ("C-p" ivy-previous-history-element)
                  ("M-." ivy-insert-current)
                  ("M-i" ivy-dispatching-done)
                  ("M-j" ivy-next-line)
                  ("M-k" ivy-previous-line)
                  ("M-o" forward-word)
                  ("M-s" ivy-done)
                  ("M-v" ivy-yank-symbol)
                  ("M-v" yank)
                  ("M-w" ivy-yank-word)))
    (define-key ivy-minibuffer-map (kbd (car cell)) (cadr cell)))

  (setq ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "
        ivy-height 6))

;; (use-package smex)
;;used by counsel-M-x (supports command history)

(use-package ivy-rich
  :ensure t
  :config
  (setcdr (assq t ivy-format-functions-alist)
          #'ivy-format-function-line)
  (ivy-rich-mode 1))

(use-package prescient
  :ensure t
  :custom
  (prescient-history-length 50)
  (prescient-save-file "~/.emacs.d/prescient-items")
  (prescient-filter-method '(fuzzy initialism regexp))
  :config
  (prescient-persist-mode 1))

(use-package ivy-prescient
  :ensure t
  :after (prescient ivy)
  :custom
  (ivy-prescient-sort-commands t)
  (ivy-prescient-retain-classic-highlighting t)
  (ivy-prescient-enable-filtering t)
  (ivy-prescient-enable-sorting t)
  :config
  (ivy-prescient-mode 1))
```


## Magit {#magit}

```emacs-lisp
(use-package magit
  :bind
  (("C-c m" . magit-status)
   ("<f11>" . magit-status))
  :config
  ;; helps to update `vc-state' info after magit action on a file
  ;; https://magit.vc/manual/magit/The-mode_002dline-information-isn_0027t-always-up_002dto_002ddate.html
  (setq auto-revert-check-vc-info t)
  (setq magit-diff-refine-hunk 'all)

  ;; kbd
  (define-keys-in magit-mode-map
    '(("q" mu-magit-kill-buffers)
      ("M-k" magit-next-line)
      ("M-i" magit-previous-line)
      ("M-j" backward-char)
      ("M-n" ivy-switch-buffer)
      ("C-n" magit-section-forward-sibling)
      ("C-p" magit-section-backward-sibling)))

  (define-prefix-command 'pk/magit-map)
  (bind-key "C-x m" 'pk/magit-map)
  (define-keys-in pk/magit-map
    '(("c" magit-checkout)
      ("l l" magit-log-current)
      ("b b" magit-blame)
      ("b c" magit-blame-cycle-style)))

  (defun mu-magit-kill-buffers ()
    "Restore window configuration and kill all Magit buffers."
    (interactive)
    (let ((buffers (magit-mode-get-buffers)))
      (magit-restore-window-configuration)
      (mapc #'kill-buffer buffers))))
```

This one is from [Manuel Uberti](http://manuel-uberti.github.io/emacs/2018/02/17/magit-bury-buffer/).

```emacs-lisp
(defun mu-magit-kill-buffers ()
  "Restore window configuration and kill all Magit buffers."
  (interactive)
  (let ((buffers (magit-mode-get-buffers)))
    (magit-restore-window-configuration)
    (mapc #'kill-buffer buffers)))
```


## Occur (package: replace) {#occur--package-replace}

```emacs-lisp
(use-package replace
  :ensure nil
  :config
  (define-keys-in occur-mode-map
    '(("j" next-line)
      ("k" previous-line)
      ("/" swiper-isearch)
      ("g" beginning-of-buffer)
      ("G" end-of-buffer)
      ("R" occur-rename-buffer)
      ("r" (lambda () (interactive) (revert-buffer)
             (text-scale-set text-scale-mode-amount))))))
```


## Projectile & Counsel-projectile {#projectile-and-counsel-projectile}

```emacs-lisp
(use-package projectile
  :config
  (projectile-mode)

  (setq projectile-completion-system 'ivy
        projectile-indexing-method 'alien
        projectile-project-search-path '("~/.dotfiles"
                                         "~/box"
                                         "~/go/src"
                                         "~/web"
                                         "~/git"))

  (dolist (file '("TAGS"
                  ".projectile"
                  ".dir-locals.el"
                  ".gitignore"
                  "requirements.txt"
                  "__init__.py"))
    (add-to-list 'projectile-globally-ignored-files file))

  (dolist (dir '("__pycache__"
                 "\.git"
                 "\.mypy_cache"))
    (add-to-list 'projectile-globally-ignored-directories dir))

  ;; do not exclude files from gitignore by default
  ;; https://github.com/bbatsov/projectile/issues/1322#issuecomment-428462811
  (setq projectile-git-command "fd . --print0 --no-ignore-vcs --color never -E \"*__pycache__*\"")

  ;; (define-key pk/ctrl-x-p-map "p" 'projectile-switch-project)
  ;; (define-key pk/ctrl-x-p-map "f" 'projectile-find-file)

  (defun pk/projectile-switch-project->treemacs ()
    (interactive)
    (projectile-switch-project)
    (when (require 'treemacs nil 'noerror)
      (treemacs-do-add-project-to-workspace
       (projectile-project-root)
       (projectile-project-name))))

  (defun pk/projectile-dwim (&optional p)
    "With prefix argument 9 find file in project, with prefix 0 find dir, with 3 run `projectile-commander', else run `projectile-switch-project'"
    (interactive "P")
    (if p (cond ((= p 9) (pk/projectile-switch-project->treemacs))
                ((= p 3) (projectile-commander))
                ((= p 0) (projectile-find-dir))
                ((= p 1) (projectile-discover-projects-in-directory
                          (read-directory-name "Start directory: ")))
                ((= p 2) (projectile-discover-projects-in-directory
                          default-directory)))
      (projectile-find-file)))

  (global-set-key (kbd "M-z") #'pk/projectile-dwim))



(use-package counsel-projectile
  :after projectile)
```


## Rainbow-delimiters {#rainbow-delimiters}

```emacs-lisp
(use-package rainbow-delimiters
  :hook ((emacs-lisp-mode lisp-interaction-mode) . rainbow-delimiters-mode))
```


## [off] Smartparens {#off-smartparens}

```emacs-lisp
;; (use-package smartparens
;;   :config
;;   (require 'smartparens-config)
;;   (add-hook 'prog-mode-hook #'show-smartparens-mode)
;;   ;; (add-hook 'prog-mode-hook #'smartparens-mode)
;;   )
```


## Switch window {#switch-window}

```emacs-lisp
(use-package switch-window
  :ensure t
  :bind (("<menu> 2" . pk/split-and-switch-b)
         ("<menu> 3" . pk/split-and-switch-r))
  :config
  (setq switch-window-shortcut-style 'qwerty)
  (setq switch-window-minibuffer-shortcut (string-to-char "m"))

  (defun pk/split-and-switch-r ()
    "Podziel okno pionowo, przejdź do drugiego okna (jeśli więcej okien aktywnych - wybierz, do którego okna; wymaga `switch-window')"
    (interactive)
    (split-window-right)
    (windmove-right))

  (defun pk/split-and-switch-b ()
    "Podziel okno poziomo, przejdź do drugiego okna (jeśli więcej okien aktywnych - wybierz, do którego okna; wymaga `switch-window')"
    (interactive)
    (split-window-below)
    (windmove-down))

  (global-set-key (kbd "<menu> 2") #'pk/split-and-switch-b)
  (global-set-key (kbd "<menu> 3") #'pk/split-and-switch-r)

  (defvar pk/prev-buffer-window nil "Previously visited window on `switch-window--list'.")

  (defun pk/jump-to-window-dwim (&optional p)
    (interactive "P")
    (let* ((win_list (switch-window--list))
           (windows (length win_list))
           (current (get-buffer-window)))
      (if (and p (<= p windows))
          (switch-window--jump-to-window p)
        (if (= windows 1)
            (progn
              (split-window)
              (switch-window)
              (switch-to-buffer (previous-buffer)))
          (if (and (member pk/prev-buffer-window win_list)
                   (not (eq current pk/prev-buffer-window)))
              (switch-window--select-window
               pk/prev-buffer-window)
            (switch-window))))
      (setq pk/prev-buffer-window current)))

  (global-set-key (kbd "M-/") 'pk/jump-to-window-dwim)
  )
```

<a id="code-snippet--pk-jump-to-window-dwim"></a>
```emacs-lisp
(defvar pk/prev-buffer-window nil "Previously visited window on `switch-window--list'.")

(defun pk/jump-to-window-dwim (&optional p)
  (interactive "P")
  (let* ((win_list (switch-window--list))
         (windows (length win_list))
         (current (get-buffer-window)))
    (if (and p (<= p windows))
        (switch-window--jump-to-window p)
      (if (= windows 1)
          (progn
            (split-window)
            (switch-window)
            (switch-to-buffer (previous-buffer)))
        (if (and (member pk/prev-buffer-window win_list)
                 (not (eq current pk/prev-buffer-window)))
            (switch-window--select-window
             pk/prev-buffer-window)
          (switch-window))))
    (setq pk/prev-buffer-window current)))

(global-set-key (kbd "M-/") 'pk/jump-to-window-dwim)
```
