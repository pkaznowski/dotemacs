+++
title = "\"init.el\" wrapper"
author = ["Piotr Kaznowski"]
draft = false
weight = 110
anchor = "wrapper"
+++

This code tangles to `~/.emacs.d/init.el` which starts initialization.


## Temporarily increase bytes for garbage collection {#temporarily-increase-bytes-for-garbage-collection}

This is inspired by [John Wiegley's](https://github.com/jwiegley/dot-emacs/blob/master/init.el#L7) config.

```emacs-lisp
(setq gc-cons-threshold 400000000
      gc-cons-percentage 0.6)

(add-hook 'after-init-hook
          `(lambda ()
             (setq gc-cons-threshold 800000
                   gc-cons-percentage 0.1)
             (garbage-collect))
          t)
```


## Cleanup appearance {#cleanup-appearance}

This is just for starters.

```emacs-lisp
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(tooltip-mode -1)
(blink-cursor-mode 0)

(setq-default cursor-type 'bar)
(setq-default cursor-in-non-selected-windows nil)
(setq inhibit-startup-message t)
(setq initial-scratch-message ";;; scratch\n\n")
```


## Set up package repositories and package manager (use-package) {#set-up-package-repositories-and-package-manager--use-package}

```emacs-lisp
(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  ;; (setq use-package-verbose t)
  (setq use-package-always-ensure t)
  (require 'use-package))
```


## Set custom file {#set-custom-file}

<a id="code-snippet--custom file"></a>
```emacs-lisp
(defconst pk/custom-file "my-custom.el" "My custom file")
(setq custom-file (expand-file-name pk/custom-file user-emacs-directory))
(load custom-file)
```


## Manage backups {#manage-backups}

Write backup files to own directory:

<a id="code-snippet--backups"></a>
```emacs-lisp
(setq backup-directory-alist
      `(("." . ,(expand-file-name
                 (concat user-emacs-directory "backups")))))
```

Write all autosave files to the `/tmp` dir:

```emacs-lisp
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Make backups of files, even when they're in version control
(setq vc-make-backup-files t)
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      delete-by-moving-to-trash t
      auto-save-default t               ; auto-save every buffer that visits a file
      ;; auto-save-timeout 20           ; number of seconds idle time before auto-save
                                        ; (default: 30)
      ;; auto-save-interval 200         ; number of keystrokes between auto-saves
                                        ; (default: 300)
      )
```


## Set bookmarks file {#set-bookmarks-file}

<a id="code-snippet--bookmarks"></a>
```emacs-lisp
(setq bookmark-default-file (expand-file-name "bookmarks" user-emacs-directory))
```


## Local variables {#local-variables}

```emacs-lisp
(setq enable-local-variables t)
```


## Set up load path {#set-up-load-path}

```emacs-lisp
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp")
(add-to-list 'load-path "~/.emacs.d/config")
(add-to-list 'load-path "~/.emacs.d/lisp")
```


## Load dot files {#load-dot-files}

This config is parcelled into smaller files.

```emacs-lisp
(defconst pk/kbd-file        "dot-kbd.el"          "Basic keybindings")
(defconst pk/basic-pkg-file  "dot-basic-pkgs.el"   "Basic packages and functions")
(defconst pk/org-file        "dot-org.el"          "Org mode setup")
(defconst pk/appearance-file "dot-appearance.el"   "Appearance, theme etc. setup")
(defconst pk/my-modes-file   "dot-custom-modes.el" "My custom modes")
(defconst pk/my-funcs-file   "dot-my-functions.el" "My custom funcs")
(defconst pk/env-file        "dot-env.el"          "Env., builtins etc.")
(defconst pk/packages-file   "dot-packages.el"     "Other packages.")
(defconst pk/client-file     "dot-client.el"       "Additional settings for emacsclient.")

(dolist (file `(,pk/kbd-file
                ,pk/appearance-file
                ,pk/env-file
                ,pk/basic-pkg-file
                ,pk/org-file
                ,pk/packages-file
                ,pk/my-modes-file
                ,pk/my-funcs-file))
  (let ((file-path (expand-file-name file (concat user-emacs-directory "config"))))
    (when (file-exists-p file-path)
      (load file-path))))

(let ((file (expand-file-name pk/client-file (concat user-emacs-directory "config"))))
  (when (and (not (display-graphic-p))
             (file-exists-p file))
    (load file)))
```


## Start server {#start-server}

```emacs-lisp
(require 'server)
(unless (file-exists-p (expand-file-name "server" server-socket-dir))
  (server-start))
```
