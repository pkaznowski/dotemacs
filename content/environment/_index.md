+++
title = "Environment"
author = ["Piotr Kaznowski"]
draft = false
weight = 140
anchor = "environment"
+++

## Frame {#frame}

```emacs-lisp
(when (string= system-type "gnu/linux")
  (setq frame-title-format
        '(:eval
          (or
           (buffer-file-name)
           (expand-file-name (buffer-name) default-directory)))))
```


## Interface {#interface}

```emacs-lisp
(setq-default fill-column 1000)
(setq-default line-spacing
              (if (member
                   (face-attribute 'default :family)
                   '("Overpass Mono"))
                  0.25 0.2))
(setq ring-bell-function 'ignore)
(setq make-pointer-invisible t)
(setq-default indicate-empty-lines nil)
(setq-default left-margin-width 1)
(global-visual-line-mode)
```


## Shrink windows {#shrink-windows}

```emacs-lisp
(global-set-key (kbd "C-x <") (lambda () (interactive) (shrink-window 10 t)))
(global-set-key (kbd "C-x >") (lambda () (interactive) (shrink-window -10 t)))
(global-set-key (kbd "C-x ,") (lambda () (interactive) (shrink-window 10)))
(global-set-key (kbd "C-x .") (lambda () (interactive) (shrink-window -10)))
```


## Behavior {#behavior}

<a id="code-snippet--behaviour"></a>
```emacs-lisp
(when (string-equal system-type "gnu/linux")
  (setq browse-url-browser-function 'browse-url-generic
        browse-url-generic-program "qutebrowser"))
(setq enable-local-variables t)
(setq vc-follow-symlinks t)
(defalias 'yes-or-no-p 'y-or-n-p) ;; y or n zamiast yes or no
(setq uniquify-buffer-name-style 'forward) ; ustawia wszystkie ścieżki z '/' zamiast '\'

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

(global-auto-revert-mode)

(setq async-shell-command-display-buffer nil)

;; (setq split-width-threshold 160)
;; (setq split-height-threshold nil)
(setq split-height-threshold 5000)
(setq split-width-threshold 110)
```


## Editing {#editing}

```emacs-lisp
(setq sentence-end-double-space nil)
(setq-default indent-tabs-mode nil)

(use-package delsel
  :defer t
  :config (delete-selection-mode t)) ;; delete region on typing
```


## Registers {#registers}

```emacs-lisp
(let ((registers '(
                   (?3 "~/.dotfiles/i3/config")
                   (?b "/tmp/scratch.org")
                   (?B "~/org/brudnopis.org")
                   (?c "~/git/pkaz/dotemacs/config.org")
                   (?C "~/.emacs.d/my-custom.el")
                   (?d "~/git/hub/heder/heder.el")
                   (?e "~/.emacs")
                   (?f "~/box/Dropbox/Wiki/Arch/arch-log.org")
                   (?g "~/org/gtd/gtd.org")
                   (?i "~/box/Dropbox/.Config/Emacs/init.org")
                   (?I "~/box/Dropbox/Dokumenty/Gtd/inbox.org")
                   (?o "~/org/gtd/oikonomia2019.org")
                   (?s "~/szk/szkoła_18-19_I.org")
                   (?t "~/box/Dropbox/Szkoła/Materiały/teksty.org")
                   (?æ "~/.config/fish/config.fish")
                   )))
  (dolist (x registers)
    (set-register (car x) `(file . ,(cadr x)))))

(define-key pk/m-spc-map "r" #'jump-to-register)
```


## Libraries {#libraries}

<a id="code-snippet--libraries"></a>
```emacs-lisp
(use-package cl        :defer t)
(use-package winner    :defer t :config (winner-mode))
(use-package elec-pair :defer t :config (electric-pair-mode t))
(use-package paren     :defer t)
(use-package fringe    :ensure nil :config (fringe-mode 0))
(use-package abbrev    :ensure nil :config
  (setq-default abbrev-mode t)
  (setq save-abbrevs 'silently))
```


## Locale, Encoding {#locale-encoding}

```emacs-lisp
(setq locale-coding-system 'utf-8)
(setq file-name-coding-system 'utf-8)
(set-locale-environment "pl_PL.UTF-8")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq system-time-locale "C") ; rozwiązuje problem kodowania time stamps
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
```


## Dashboard {#dashboard}

```emacs-lisp
(use-package dashboard
  :init
  (dashboard-setup-startup-hook)
  :config
  (add-hook 'dashboard-mode-hook
            (lambda ()
              (visual-line-mode -1)
              (toggle-truncate-lines 1)
              (setq left-margin-width 1)))

  (setq dashboard-banner-logo-title
        (format "Emacs: %s of %s · Org-mode: %s" ;; Init time: %s
                emacs-version
                (substring (emacs-version) -10)
                (org-version)
                ;; (emacs-init-time)
                ))

  (set-face-attribute 'widget-button nil :weight 'normal)
  (set-face-attribute 'dashboard-banner-logo-title nil
                      :foreground (face-attribute 'font-lock-constant-face
                                                  :foreground)
                      :weight 'bold)

  (setq dashboard-footer "")
  (setq dashboard-footer-messages '(""))
  (setq dashboard-startup-banner 'logo) ;; 'official // "path/to/your/image.png"
  (setq dashboard-items '((bookmarks . 5)
                          (projects  . 5)
                          (recents   . 5)))
  ;; add agenda
  ;; (add-to-list 'dashboard-items '(agenda) t)

  (define-keys-in dashboard-mode-map
    '(("/" swiper)
      (";" avy-goto-char-2)
      ("I" ibuffer)
      ("S" (lambda () (interactive) (switch-to-buffer "*scratch*")))
      ("a" pk/get-org-agenda-dwim)
      ("d" dired)
      ("f" counsel-find-file)
      ("h" dashboard-previous-section)
      ("j" widget-forward)
      ("k" widget-backward)
      ("l" dashboard-next-section)
      ("n" ivy-switch-buffer)
      ("o" (lambda () (interactive) (unless (thing-at-point 'filename)
                                      (forward-char 4))
             (widget-button-press (point))))
      ("<backtab>" dashboard-previous-section)
      ("<tab>" dashboard-next-section)))

  (dashboard-insert-shortcut "b" "Bookmarks:")
  (dashboard-insert-shortcut "r" "Recent Files:")
  (dashboard-insert-shortcut "p" "Projects:")

  ;; [[[[file:~/git/pkaz/dotemacs/config.org::dashboard-custom-sections][dashboard-custom-sections]]][dashboard-custom-sections]]
  (defvar dashboard-easy-access nil
    "Assoc list of imprtant file names and their paths to use in emacs dashboard")
  (defvar dashboard-shool-files-alist nil
    "Assoc list of my shool file names and their paths to use in emacs dashboard")

  ;; set school files dynaically depending on semester etc.
  (let* ((dir "~/szk")
         (month (string-to-number (format-time-string "%m")))
         (year (string-to-number (format-time-string "%y")))
         (sem (if (and (> month 1) (< month 9)) "_II" "_I"))
         (year (if (and (> month 1) (< month 9))
                   (format "%s-%s" (1- year) year)
                 (format "%s-%s" year (1+ year))))
         (subjects
          (list "gramatyka" "retoryka" "dialektyka" "filozofia_artes" "szkoła")))
    (setq dashboard-school-files-alist (list))
    ;; clear old list
    (dolist (sub subjects)
      (let ((file (expand-file-name
                   (concat sub "_" year sem ".org")
                   dir)))
        (when (or (file-exists-p file)
                  (file-symlink-p file))
          (message "%s" (cons sub file))
          (add-to-list 'dashboard-school-files-alist (cons sub file))))))

  (defun pk/dashboard--default-easy-access ()
    (setq dashboard-easy-access nil)
    (dolist (element (list
                      (cons "Szkoła @dp" "~/box/Dropbox/Szkoła")
                      (cons "pgm @schole" "~/web/schole/pgm")
                      (cons "iliada @schole" "~/web/schole/iliada")
                      (cons "wiki @dp" "~/box/Dropbox/Wiki")
                      (cons "emacs.d @home" "~/.emacs.d")
                      (cons "scr @home" "~/scr")
                      ))
      (add-to-list 'dashboard-easy-access element)))

  (pk/dashboard--default-easy-access)

  (defun pk/dashboard-add-to-easy-access (p)
    (interactive "P")
    (if (not p)
        (let* ((file (or (buffer-file-name)
                         default-directory))
               (name (buffer-name)))
          (add-to-list 'dashboard-easy-access (cons name file))
          (when (get-buffer "*dashboard*")
            (dashboard-refresh-buffer))
          (message "File %s added to *dashboard*" file))
      (pk/dashboard--default-easy-access)
      (dashboard-refresh-buffer)
      (message "*dashboard* scratchpad cleared up")))

  (defun pk/dashboard-insert-easy-access (list-size)
    "Add the list of important files to `dashboard'"
    (dashboard-insert-section
     "Easy access:"
     (dashboard-subseq
      (mapcar
       (lambda (x) (car x)) dashboard-easy-access)
      0 10)
     list-size
     "i"
     `(lambda (&rest ignore)
        (find-file-existing
         (assoc-default ,el dashboard-easy-access)))
     (abbreviate-file-name el)))

  (defun pk/dashboard-insert-schoolfiles (list-size)
    "Add the list school files to `dashboard'"
    (dashboard-insert-section
     "Szkoła:"
     (dashboard-subseq
      (mapcar (lambda (x) (car x))
              dashboard-school-files-alist)
      0 10)
     list-size
     "s"
     `(lambda (&rest ignore)
        (find-file-existing
         (assoc-default ,el dashboard-school-files-alist)))
     (abbreviate-file-name el)))

  (dolist (generator (list
                      '(school . pk/dashboard-insert-schoolfiles)
                      '(scratchpad . pk/dashboard-insert-easy-access)
                      ))
    (add-to-list 'dashboard-item-generators generator)
    (add-to-list 'dashboard-items (list (car generator))))
  ;; dashboard-custom-sections ends here
  ;; [[[[file:~/git/pkaz/dotemacs/config.org::dashboard-dwim][dashboard-dwim]]][dashboard-dwim]]
  (defun pk/dashboard-dwim ()
    "Goto to *dashboard*, if doesn't exist, create and go"
    (interactive)
    (unless (get-buffer "*dashboard*")
      (dashboard-insert-startupify-lists))
    (dashboard-refresh-buffer)
    (switch-to-buffer "*dashboard*"))

  (define-key pk/m-spc-map "`" #'pk/dashboard-dwim)
  ;; dashboard-dwim ends here
  )
```

<a id="code-snippet--dashboard-custom-sections"></a>
```emacs-lisp
(defvar dashboard-easy-access nil
  "Assoc list of imprtant file names and their paths to use in emacs dashboard")
(defvar dashboard-shool-files-alist nil
  "Assoc list of my shool file names and their paths to use in emacs dashboard")

;; set school files dynaically depending on semester etc.
(let* ((dir "~/szk")
       (month (string-to-number (format-time-string "%m")))
       (year (string-to-number (format-time-string "%y")))
       (sem (if (and (> month 1) (< month 9)) "_II" "_I"))
       (year (if (and (> month 1) (< month 9))
                 (format "%s-%s" (1- year) year)
               (format "%s-%s" year (1+ year))))
       (subjects
        (list "gramatyka" "retoryka" "dialektyka" "filozofia_artes" "szkoła")))
  (setq dashboard-school-files-alist (list))
  ;; clear old list
  (dolist (sub subjects)
    (let ((file (expand-file-name
                 (concat sub "_" year sem ".org")
                 dir)))
      (when (or (file-exists-p file)
                (file-symlink-p file))
        (message "%s" (cons sub file))
        (add-to-list 'dashboard-school-files-alist (cons sub file))))))

(defun pk/dashboard--default-easy-access ()
  (setq dashboard-easy-access nil)
  (dolist (element (list
                    (cons "Szkoła @dp" "~/box/Dropbox/Szkoła")
                    (cons "pgm @schole" "~/web/schole/pgm")
                    (cons "iliada @schole" "~/web/schole/iliada")
                    (cons "wiki @dp" "~/box/Dropbox/Wiki")
                    (cons "emacs.d @home" "~/.emacs.d")
                    (cons "scr @home" "~/scr")
                    ))
    (add-to-list 'dashboard-easy-access element)))

(pk/dashboard--default-easy-access)

(defun pk/dashboard-add-to-easy-access (p)
  (interactive "P")
  (if (not p)
      (let* ((file (or (buffer-file-name)
                       default-directory))
             (name (buffer-name)))
        (add-to-list 'dashboard-easy-access (cons name file))
        (when (get-buffer "*dashboard*")
          (dashboard-refresh-buffer))
        (message "File %s added to *dashboard*" file))
    (pk/dashboard--default-easy-access)
    (dashboard-refresh-buffer)
    (message "*dashboard* scratchpad cleared up")))

(defun pk/dashboard-insert-easy-access (list-size)
  "Add the list of important files to `dashboard'"
  (dashboard-insert-section
   "Easy access:"
   (dashboard-subseq
    (mapcar
     (lambda (x) (car x)) dashboard-easy-access)
    0 10)
   list-size
   "i"
   `(lambda (&rest ignore)
      (find-file-existing
       (assoc-default ,el dashboard-easy-access)))
   (abbreviate-file-name el)))

(defun pk/dashboard-insert-schoolfiles (list-size)
  "Add the list school files to `dashboard'"
  (dashboard-insert-section
   "Szkoła:"
   (dashboard-subseq
    (mapcar (lambda (x) (car x))
            dashboard-school-files-alist)
    0 10)
   list-size
   "s"
   `(lambda (&rest ignore)
      (find-file-existing
       (assoc-default ,el dashboard-school-files-alist)))
   (abbreviate-file-name el)))

(dolist (generator (list
                    '(school . pk/dashboard-insert-schoolfiles)
                    '(scratchpad . pk/dashboard-insert-easy-access)
                    ))
  (add-to-list 'dashboard-item-generators generator)
  (add-to-list 'dashboard-items (list (car generator))))
```

<a id="code-snippet--dashboard-dwim"></a>
```emacs-lisp
(defun pk/dashboard-dwim ()
  "Goto to *dashboard*, if doesn't exist, create and go"
  (interactive)
  (unless (get-buffer "*dashboard*")
    (dashboard-insert-startupify-lists))
  (dashboard-refresh-buffer)
  (switch-to-buffer "*dashboard*"))

(define-key pk/m-spc-map "`" #'pk/dashboard-dwim)
```


## Ibuffer {#ibuffer}


### ibufer base {#ibufer-base}

```emacs-lisp
(use-package ibuffer
  :defer 0.1
  :config
  (setq ibuffer-expert t)
  ;; (setq ibuffer-use-header-line nil) ; doesn't work
  (add-hook 'ibuffer-mode-hook
            (lambda ()
              (ibuffer-auto-mode 1)
              (ibuffer-switch-to-saved-filter-groups "default")))

  (add-hook 'ibuffer-hook (defun pk/ibuffer-custom-hooks ()
                            (unless truncate-lines (toggle-truncate-lines))
                            (hl-line-mode 1)))

  (define-keys-in ibuffer-mode-map
    '(("/" swiper)
      (";" pk/ibuffer-dired-dwim)
      ("K" pk/kill-process-dwim)
      ("L" toggle-truncate-lines)
      ("h" ibuffer-toggle-filter-group)
      ("j" next-line)
      ("k" previous-line)
      ("l" ibuffer-visit-buffer)
      ("n" ibuffer-jump-to-buffer)
      ("m" pk/ibuffer-magit-status)
      ("p" pk/ibuffer-projectile)
      ("u" ibuffer-unmark-backward)
      ("M-n" ivy-switch-buffer)
      ("M-o" forward-word)
      ("M-z" pk/projectile-dwim)
      ("M->" pk/ibuffer-jump-to-last)
      ("M-<" pk/ibuffer-jump-to-first)
      ("M-RET" pk/ibuffer-toggle-filter-groups)
      ("S-SPC" ibuffer-unmark-forward)
      ("SPC" ibuffer-mark-forward)))

  (defun pk/ibuffer-dired-dwim ()
    (interactive)
    (let ((file (buffer-file-name (ibuffer-current-buffer))))
      (dired (if file (file-name-directory file) default-directory))
      (dired-goto-file file)))

  (defun pk/ibuffer-magit-status ()
    (interactive)
    (let* ((file (buffer-file-name (ibuffer-current-buffer)))
           (git (vc-git-responsible-p (or file ""))))
      (if (and file git)
          (magit-status git)
        (message "Buffer is not visiting a %s"
                 (if file "git repository" "file")))))

  (defun pk/ibuffer-jump-to-last ()
    (interactive)
    (ibuffer-jump-to-buffer
     (buffer-name
      (car (car (last
                 (ibuffer-current-state-list)))))))

  (defun pk/ibuffer-jump-to-first ()
    (interactive)
    (ibuffer-jump-to-filter-group
     (car (car (ibuffer-current-filter-groups-with-position)))))

  (defun pk/ibuffer-toggle-filter-groups (&optional p)
    "Unless all filter groups already hidden, hide all filter groups.
  With prefix argument P show all filter groups."
    (interactive "P")
    (let ((groups (mapcar
                   'car (ibuffer-current-filter-groups-with-position))))
      (setq ibuffer-hidden-filter-groups
            (if (or p (equal groups ibuffer-hidden-filter-groups))
                nil groups))
      (ibuffer-update nil t)))

  (defun pk/ibuffer-projectile ()
    (interactive)
    (let* ((file (buffer-file-name (ibuffer-current-buffer)))
           (root (if file
                     (projectile-project-root
                      (file-name-directory file))
                   nil)))
      (if root
          (projectile-ibuffer-by-project root)
        (message "Buffer doesn't adhere to any project")))))
```


### ibuffer extensions {#ibuffer-extensions}

<a id="code-snippet--ibuffer ext"></a>
```emacs-lisp
(use-package ibuf-ext
  :ensure nil
  :after ibuffer
  :config
  (setq ibuffer-saved-filter-groups
        (quote (("default"
                 ("*ERRORS*"
                  (or (name . ".* Error .*")))
                 (".LOGS"
                  (or (name . "^\\*lsp-log\\*$")))
                 (".EMACS"
                  (or (name . "^\\*scratch\\*$")
                     (name . "^\\*Messages\\*$")
                     (name . "^\\*Buffer List\\*$")
                     (name . "^\\*Help\\*$")
                     (name . "^\\*dashboard\\*$")
                     (name . "\\.el\\.gz$")
                     (mode . calc-mode)))
                 (".DIRED" (mode . dired-mode))
                 ("DEBUGGING"
                  (or
                   (mode . dap-server-log-mode)
                   (name . "^\\*DAP Templates\\*$")
                   (name . "^\\*Python :: Run Configuration<[0-9]*> out\\*$")))
                 (".GTD & NOTES"
                  (or (name . "^\\*Calendar\\*$")
                     (name . "^diary.org$")
                     (name . "^\\*Org Agenda\\*$")
                     (name . "^notes.org$")
                     (name . "^\\*notes\\*$")
                     (name . "^gtd.org$")
                     (name . "^kalendarz.org$")
                     (name . "^agenda.org$")
                     (name . "^inbox.org$")
                     (name . "^links.org$")))
                 (".SZKOŁA"
                  (or (name . "^filozofia\.*.org$")
                     (name . "^retoryka\.*.org$")
                     (name . "^dialektyka\.*.org$")
                     (name . "^gramatyka\.*.org$")
                     (name . "^szkoła\.*.org$")))
                 (".CONFIG"
                  (or (mode . conf-space-mode)
                     (mode . conf-unix-mode)
                     (mode . conf-xdefaults-mode)
                     (name . "\\.emacs$")
                     (name . "\\.gitignore$")
                     (name . "\\.toml$")
                     (name . "\\Xresources$")
                     (name . "^config$")
                     (name . "^config.def.h$")
                     (name . "^config.h$")
                     (name . "^config.org$")
                     (name . "^config.py$")
                     (name . "^new-config\.org$")
                     (name . "^init.org$")))
                 ("BASH" (or (mode . sh-mode) (name . "sh\\*$")))
                 ("BOOKMARKS" (mode . bookmark-bmenu-mode))
                 ("C" (mode . c-mode))
                 ("C++" (mode . c++-mode))
                 ("CLOJURE"
                  (or (mode . clojure-mode)
                     (mode . cider-repl-mode)
                     (mode . cider-stacktrace-mode)
                     (name . "^\\*nrepl-server")
                     (name . "^\\*clojure-lsp\\(::stderr\\)*\\*$")))
                 ("ELISP"
                  (or (mode . emacs-lisp-mode)
                     (mode . inferior-emacs-lisp-mode)
                     (mode . lisp-interaction-mode)
                     (mode . ert-results-mode)
                     (name . "^\\*lispy-message\\*$")))
                 ("FISH"
                  (or (mode . fish-mode)
                     (name . "\\.fish\\*$")))
                 ("GO"
                  (or (mode . go-mode)
                     (name . "\\.go\\*$")))
                 ("HTML & (S)CSS"
                  (or (mode . html-mode)
                     (mode . css-mode)
                     (mode . scss-mode)
                     (mode . mhtml-mode)))
                 ("IMAGE"
                  (or (name . "jpg\\*$")
                     (name . "png\\*$")
                     (mode . image-mode)))
                 ("INFO"
                  (or (mode . Info-mode)))
                 ("JAVA-SCRIPT"
                  (mode . js-mode))
                 ("LaTeX"
                  (or (mode . latex-mode)
                     (mode . TeX-special-mode)
                     (mode . TeX-output-mode)
                     (name . "tex\\*$")
                     (name . "^\\*Org PDF LaTeX Output\\*$")))
                 ("MARKDOWN"
                  (or (mode . markdown-mode)
                     (mode . gfm-mode)
                     (name . "\\.md\\$")
                     (name . "\\.markdown\\$")))
                 ("OCCUR" (mode . occur-mode))
                 ("ORG"
                  (or (mode . org-mode)
                     (name . "^\\*Org Clock\\*$")))
                 ("PDF" (mode . pdf-view-mode))
                 ("PYTHON"
                  (or (mode . python-mode)
                     (mode . inferior-python-mode)
                     (name . "\\*Python Doc\\*")
                     (name . "\\*pyls\\(::stderr\\)*\\*")))
                 ("RSS" (name . "elfeed"))
                 ("RUST"
                  (or (mode . rust-mode)
                     (name . "\\.rs\\*$")))
                 ("SENT" (name . ".sent$"))
                 ("SHELL"
                  (or (mode . term-mode)
                     (mode . shell-mode)
                     (mode . process-menu-mode)
                     (mode . eshell-mode)
                     (name . "^\\*Shell Command Output\\*$")))
                 ("WWW" (mode . eww-mode))
                 ("YAML" (mode . yaml-mode))
                 ("MAGIT"
                  (or (mode . magit-mode)
                     (mode . magit-process-mode)
                     (mode . magit-diff-mode)
                     (mode . magit-status-mode)))
                 ("PROJECTS"
                  (or (name . " Buffers\\*$")))))))

  (setq ibuffer-show-empty-filter-groups nil)
  (add-to-list 'ibuffer-never-show-predicates "^diary$")
  (add-to-list 'ibuffer-never-show-predicates "^\\*Backtrace\\*$"))
```


### ibuffer helper funcs {#ibuffer-helper-funcs}

```emacs-lisp
(defun pk/ibuffer-dired-dwim ()
  (interactive)
  (let ((file (buffer-file-name (ibuffer-current-buffer))))
    (dired (if file (file-name-directory file) default-directory))
    (dired-goto-file file)))

(defun pk/ibuffer-magit-status ()
  (interactive)
  (let* ((file (buffer-file-name (ibuffer-current-buffer)))
         (git (vc-git-responsible-p (or file ""))))
    (if (and file git)
        (magit-status git)
      (message "Buffer is not visiting a %s"
               (if file "git repository" "file")))))

(defun pk/ibuffer-jump-to-last ()
  (interactive)
  (ibuffer-jump-to-buffer
   (buffer-name
    (car (car (last
               (ibuffer-current-state-list)))))))

(defun pk/ibuffer-jump-to-first ()
  (interactive)
  (ibuffer-jump-to-filter-group
   (car (car (ibuffer-current-filter-groups-with-position)))))

(defun pk/ibuffer-toggle-filter-groups (&optional p)
  "Unless all filter groups already hidden, hide all filter groups.
With prefix argument P show all filter groups."
  (interactive "P")
  (let ((groups (mapcar
                 'car (ibuffer-current-filter-groups-with-position))))
    (setq ibuffer-hidden-filter-groups
          (if (or p (equal groups ibuffer-hidden-filter-groups))
              nil groups))
    (ibuffer-update nil t)))

(defun pk/ibuffer-projectile ()
  (interactive)
  (let* ((file (buffer-file-name (ibuffer-current-buffer)))
         (root (if file
                   (projectile-project-root
                    (file-name-directory file))
                 nil)))
    (if root
        (projectile-ibuffer-by-project root)
      (message "Buffer doesn't adhere to any project"))))
```


## Emacsclient {#emacsclient}

```emacs-lisp
(unless (window-system)
  (global-unset-key (kbd "M-[")))
```
