#!/bin/bash

# Path:        ~/git/lab/dotemacs/tangle-config-org.sh
# Created:     2019-03-02, 16:13    @x200
# Doc:         tangle config.org file

TANGLE_DEFAULT=$HOME/git/kaz/dotemacs/config.org
SERVER_DIR=/tmp/emacs1000
SOCKET=term
if [[ $(which notify-send) ]]; then
    NOTIFY1="notify-send \"Emacs config\" \"Tangling config files complete!\""
    NOTIFY2="notify-send \"Emacs config\" \"Server '$SOCKET' restarted\""
else
    NOTIFY1=
    NOTIFY2=
fi

usage(){
    cat <<EOF  
usage $0: [-h] [-svt] [-fF <file>]

Without arguments given script will tangle config.org file, 
check python venv at 'user-emacs-directory' and restart
Emacs server.

options:
 -h         print this message
 -f <file>  performs all actions for given <file> path
 -F <file>  like -f but only tangles
 -s         restart Emacs server
 -t         tangle config.org file 
EOF
    exit
}

check_script_path(){
    if [[ ! -f $TANGLE_FILE ]]; then
        cat <<EOF
 
 Emacs config 
 ============
 Path of $TANGLE_FILE incorrect.
 Please verify path to *config.org* file and pass it as argument to this script.

EOF
        exit 1
    fi
}

setup_env() {
    EMACS_DIR=$HOME/.emacs.d
    CONF=$EMACS_DIR/config
    CUSTOM=$EMACS_DIR/my-custom.el
    EMACS_VENV=$EMACS_DIR/.venv
    DOTEM=$HOME/.emacs

    [[ -d  $CONF ]] || mkdir -p $CONF
    [[ -f $CUSTOM ]] || touch $CUSTOM
}

tangle() {
    TANGLE_FILE=${TANGLE_FILE:-$TANGLE_DEFAULT}
    
    cat << EOF 
 
 Emacs config
 ============
 Tangling elisp code from *$(basename $TANGLE_FILE)*...

EOF
    
    emacs -Q --batch --eval \
          "(let ((my-config-dir (car command-line-args-left))
                 (tangle-src (cadr command-line-args-left)))
             (require 'ob-tangle)
             (with-current-buffer (find-file-noselect tangle-src)
               (org-babel-tangle)))" $CONF $TANGLE_FILE \
                   && if [[ -f $DOTEM ]]; then rm $DOTEM; fi \
                          && eval "${NOTIFY1}"
}

server_start() {
    cat << EOF

 Emacs config
 ============
 (Re)starting server *$SOCKET* at $SERVER_DIR/ ...

EOF

    if [[ -S $SERVER_DIR/$SOCKET ]]
    then
        emacsclient -s $SOCKET --eval "(save-some-buffers t)" \
            && rm $SERVER_DIR/$SOCKET
    fi

    ps aux | rg "emacs --daemon=term" | rg -v rg | awk '{print $2}' | xargs kill -9
    emacs --daemon=$SOCKET && eval "${NOTIFY2}"
}

pyvenv() {
    cat << EOF 
 
 Emacs config
 ============
 Checking python virtual environment for Emacs...

EOF

    if [ ! -d $EMACS_VENV ]; then
        echo $EMACS_VENV 
        python -m venv $EMACS_VENV
    fi
    
    source $EMACS_VENV/bin/activate
    pip install -r requirements.txt
    
    echo -e "\n ...done!"
}

main() {
    full_build(){
        tangle
        pyvenv
        server_start
    }
    setup_env
    if [ -z $1 ]; then
        full_build
    else
        while getopts "hstvf:" option; do
            case ${option} in
                h) usage ;;
                f) TANGLE_FILE=$"OPTARG"
                   check_script_path
                   full_build
                   ;;
                F) TANGLE_FILE=$"OPTARG"
                   check_script_path
                   tangle
                   exit ;;
                t) tangle ;;
                v) pyvenv ;;
                s) server_start ;;
                *) usage ;;
            esac
        done
    fi
}

main $@
